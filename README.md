# FlexRAN
A platform for Software-Defined Radio Access Networks

FlexRAN is composed of two parts:

* The FlexRAN real-time master controller
* The FlexRAN agent running at the eNodeB

This repository is meant to act as a central point from where both the code of
the controller and the agent can be obtained. 

To request access to the repository (both controller and agent) please send a
request to flexran_subscribe@lists.eurecom.fr.

To obtain the code, simply clone this repository and from its main directory run
```
git submodule init
```

Then choose to load the appropriate submodule depending on the deployment node 
(agent or controller).

## FlexRAN real-time controller

For the FlexRAN controller simply run:
```
git submodule update --remote controller
```
The source code of the controller will be cloned from the appropriate repository
and can be found in the **controller** directory.

## FlexRAN agent
For the FlexRAN agent simply run:
```
git submodule update --remote agent
```
The source code of the agent will be cloned and can be found in the 
**agent** directory.

## FlexRAN installation and execution
For instructions about installing and running FlexRAN, please see the tutorials
in the wiki.